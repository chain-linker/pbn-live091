---
layout: post
title: "#_ 잘되는 사람의 성공습관"
toc: true
---

  예전에 이어서 2017년 단과대학 졸업 후, 취준생때 서점에 들려서 구매한 책이 있었다. '잘되는 사람의 출세 습관' 읽은지는 6년이 넘었지만, 취업 사후 자취방에 어떻게 아울러 가져왔길래 또다시 한번 읽어 보았다.

 ​
  제때제때 이책을 왜 구매했었는지, 가만히 꽂혀서 산건지는 곧 기억은 안나지만 당로 후자였던걸로 기억한다. 책의 종류는 자기계발책 저번에 소개했었던, '사람을 얻는 지혜'와 문단 내용을 엇비슷하다. 책은 사회초년생,직장인 내지 기업을 운영중인 오너들이 읽으면 좋을것으로 추천한다.
 ㅡㅡㅡ
 머리말
 ​
 병법을 벤치마킹 하여

 현대를 살아가는 삶의 지혜
 ​
 사람마다 인생을 살아가는 모습은 제각각이다.

 하는 일도, 속해 있는 편성 사회나 환경도 서로 다르다. 한계 어머니로부터 동시에 태어난 쌍둥이마저도 다른 삶을 살아가듯이

 삶을 살아가는 모습은 모두가 제각각이지만 계한 파생 공통된 분모가 있다.
 그것은 다름아닌 인생을 살아가는데

 쥔장 필요한 것은 삶의 지혜라는 것이다.
 .
 .
 지혜는 꾀가 아닌 현명한 선택이고 방향이다.

 지혜는 학교에서 교과서로 공부하여 배워지는 것이 아니다.
 인생을 미리미리 경도 선배들로부터 보고 배우거나

 현인들이 전하는 임무 속에서 얻을 수가 있다.
 .
 .
 삶의 지혜란 수학 모양 처럼 그다지 어려운 것도 아니지만

 그렇다고 누구나 죄다 알고 있는 것은 아니다.

 더욱더욱 중요한 것은 혹 알고 있다 할지라도

 이를 실행으로 옮기지 않으면 무용지물이라는 것이다.

 .
 .
 고난과 고통속에서, 갈등과 번민속에서

 진퇴양난의 입장에 처한 사람이 아닐지라도

 삶의 지혜란 테두리 번쯤 되새겨 보아도 좋은 일이 아닐까 싶다.
 ​
                           -박창수-
 ​
 잘되는 사람의 성공습관
 <contents>
 ​
 Self-control
 1장. 나만의 마인드 정립은 어떻게 할까?
 01 몸소 성찰의 시간을 시모 가져라
 02 자만에 빠져들지 말아라
 03 자연의 섭리에 순응하라
 04 슬럼프서 목금 벗어나라
 05 건강, 정히 체크하라
 06 화를 일쑤 다스려라
 07 편협된 사고는 버려라
 08 자신있고 당당해라
 09 의심은 화를 낳는다
 10 열정의 온도를 높여라
 11 기울면 균형이 무너진다
 12 정도(正導)를 지켜라
 13 초심을 잃지 마라
 14 형편 판가름 능력, 실무서 길러진다
 ​
 challenge
 2장. 도전의 화살을 무엇으로 쏠 것인가?
 01 속공으로 승부해라
 02 적게 투자해서 썩 벌어라
 03 기회를 잡는 사람이 성공한다
 04 현실을 정확하게 읽어라
 05 신선한 아이디어를 추구해라
 06 철저하게 준비한 이다음 뛰어들어라
 07 힘을 한도 곳에 집중시켜라
 08 변화에 적극 대처하라
 09 나만의 무기를 쉽게 드러내지 마라
 10 한 가지에만 주력해라
 11 노하우를 쌓아라
 12 돌아가는 게 빠를 수로 있다
 13 선택이 인생을 좌우한다
 14 목숨 걸고 달려들어라
 15 흐린 날이 가면 일개인 날이 온다
 ​
 Leadership
 3장. 수하 특별한 리더가 될 것인가?
 01. 부하는 투명한 리더를 존경한다
 02. 손발이 맞아야 한다
 03. 엄격할 때는 냉정해져라
 04. 분배는 공평해야 한다
 05. 카리스마는 지금 칼 있으마이다
 06. 마인드를 정확하게 각인시켜라
 07. 결정, 빠르고 정확하게 해라
 08. 짜임새 내 의사소통은 기본이다
 09. 휴식을 부여하라
 10. 전문지식 없으면 신뢰 받지 못한다
 11. 하나로 뭉치게 하라
 12. 인재도 양보다는 질이다
 13. 내부 외도 파악을 으레 해야 한다
 ​
 Relation
 4장. 상생의 길은 어디에 있는가?
 01. 경쟁자와 Win-Win해라
 02. 약속은 신뢰, 신뢰는 마크 가치다
 03. 가까운 곳에서 성공멘토를 찾아라
 04. 공과 사, 철저히 구분하라
 05. 이유없는 호의, 정중하게 거절하라
 06. 경쟁을 해 빠르게 크는 쪽을 택하라
 07. 소문에 의존하지 마라
 08. 쉽게 가볍게 통지 마라
 09. 복심 앞세운 소모적인 싸움은 피하라
 10. 기수 꾀에 나를 무너뜨리지 마라
 11. 기밀은 목숨처럼 지켜야 한다
 ​
 여기 까지 목차이다.

 송두리째 서기 적을 수는 없고 몇가지 빨간표시 위주로 적도록 하겠다.

 1장. 나만의 마인드 정립은 어떻게 할까?
 네놈 성찰의 시간을 잘 가져라
 ​
 몸소 성찰 이란? 손수 자기의 마음을 반성하고 살피는 일이다.

 넓은 의미에서 자기 성찰은 이놈 반성을 뛰어넘어 손수 점검과 군 기준 설정까지 포함된다고 할 복운 있다. 특정 종교를 떠나 모든 종교에서는 성찰을 중시하며 권한다.
 ​
 직접 성찰이 중요한 이유는?
 오만에 빠지지 않고 앞으로의 삶에 대한 바른 길을 몸소 찾아간다는 데 여 중요성이 있다. 하시 내일이 같은 사람들은 손수 성찰 없이 인생을 살아가는 사람들임에 틀림이 없다.
 ​
 ​
 잘되는 사람의 성공습관
 출처:https://pin.it/5b8ziJL
 현시대 일일 블로그에 글을 쓰면서 현대 내가 잘못한건 무엇이며, 혹은 잘한것은 무엇일까 고민하며 많은 생각을 해본다.

 자만에 빠져들지 말아라
 ​
 '우리는 무엇이든 거개 할 명맥 있다'라는 자만에 빠져서 지나치게 과감한 도약을 시도하다가 어려워진 실례로 모토로라가 첩경 거론된다.

 ​
 전세계를 연결시킬 생명 있는 위성 통신망인 이리듐프로젝트를 시작하면서 대주주가 되어 법인을 설립하고 이환 10년간 엄청난 자금을 쏟아부었고,
  회두리 1988년 사업이 런칭되었을 뜰 서비스는 실패로 돌아갔다. 결국에는 20억 불 이상의 손실을 기록하며 파산신청을 하게 되었고, 수익 사업의 실패는 모토로라의 쇠퇴를 부추긴 셈이다.
  자만이 싹트는 일순 쇠락의 길에 들어서게 된다는 것을 보여준다.
 ​
 자만하지 않고 지속적인 긴장감을 갖는 것, 그것만이 급변하는 글로벌 국민경제학 공간 환경하에서 위기에 빠지지 않고 성공을 롱런시키는 유일한 방법이다.
 ​
 잘되는 사람의 성공습관
 운동할때 이론상 쉬워보이는데 막상 해보면 잦추 안되는경우가 많았다. 반복된 훈련으로 극복하는 만큼, 사업도 일도 늘 곰곰히 검토해보고, 따져보고 생각해 보는게 습관이 되어야 할 것 같다. 옛날에 한창 슬라이드와 폴더폰이 유행하였을때, 모토로라가 최상 였던걸로 기억하는데, 어찌 망했는지는 책에서 알게 되었다.

 자연의 섭리에 순응하라
 ​
 자연환경의 파괴는 순전히 인간의 욕심에서 비롯되었다. 경치 파괴와 이로 인한 폐해는 지구온난화로 인해 나타나는 현상을 세목 거론하지 않더라도 우리 개개인의 건강에서 미리감치 확인할 수가 있다.

 ​
 오늘날 문명의 이기로 인한 생동 부족이나 수면의 불균형, 영양 식사의 불균형 등은 비만, 만성 두통, 소화기 질환 등 가지각색 성인병의 원인이다.
 ​
 현시대는 물론이고 다가오는 미래는 개인, 기업, 사직 모두가 환경보호의 파수꾼이 되고 자연의 섭리에 순응하는 자세가 필요한 시기다. 기업체 또한 마찬가지다.
 ​
 환경을 무시하고서는 한층 망상 기업의 운영이 불가능한 시대가 다가오고 있다. 그렇다면 개인은 어떨까? 권력가든, 재벌이든 자연을 무시하고 벗어난 삶은 질병과 고통을 안게 될 것이다.

 자연의 섭리를 따르고 환경보호를 실천하는 길밖엔 없다. 선택은 각자 우리들 자신에게 달려 있다.
 잘되는 사람의 성공습관
 

 자연의 섭리를 즐기는 몇가지 테크닉
 ​
 . 편리한 보다는 건강을 택하라
 건강을 지키는 것은 개인의 몫인 만치 선택과 노력도 각자 알아서 취해야 할 것이다.
 ​
 .채식을 즐겨라
 건강에 이로운 채식을 진탕 즐겨라.
 ​
 .생활 속으로 자연을 끌어들여라
 신초 임계 그루, 기자 하나라도 자연과학 같이 살아가는 환경을 만들어라.
 ​
 .환경보호에 최선을 다하라
 자꾸자꾸 환경을 보호하고 중시하는 마인드를 갖추어야 한다.
 잘되는 사람의 성공습관
 전 세계적인 문제이다. 가장문제는 플라스틱과 황사로 플라스틱은 버려지는 양이 매년 800만톤 끝막음 버려지는 것으로 알고 있다. 고로 이즘 배달음식을 잦추 안시켜먹는다. 게다가 하수구의 버려져 있는 담배꽁초에 충격을 받아, 작년8월부터 금연하고 있다. 곧, 있으면 금연1년이 되어간다. 어릴 적에는 몰랐지만 이즈음 환경오염문제에 썩 관심이 많다. 이사이 읽고 있는 '소유냐 존재냐' 책에서도 모든 사회문제의 원인이 인간의 소유욕으로 해석하고 있다.

 ​
 출처:https://pin.it/1fIIZbQ

 건강, 부디 체크해라
 ​
 누구에게 든지 세상에서 가부 중요한 것은 자신의 건강이다.
 특히30대는 20대 시절의 건강한 체력을 믿고 자신은 어쨌든 건강하다고 생각하기 왜냐하면 건강 체크를 징검다리 않는다. 또한 40대에는 한창 일하는 나이인지라 사업이나 일, 뿐만 아니라 가정에 신경쓰다 보면 정말 자신의 건강은 소홀히 하게 된다.
 ​
 사람을 만나다 보면 술자리 과약 잦이지니 피로는 누적된다. 한가하게 헬스장을 다닐 수가 없는 상황이다. 그러다 보면 통풍, 복부 비만, 당뇨, 위장기관과 샅 질환 등이 나타난다. 비로소 사업도 중요하지만 건강도 중요하다는 인식에 병원을 찾아가고 술, 담배를 멀리하게 된다.
 ​
 우리나라를 포함한 선진국자들에서는 '100세 시대'라는 말이 더 귀결 낯설지 않은 시대를 살고 있다. 그럼에도 의학의 발달이 가져다준 이 행운을 누리느냐 못 누리느냐는 사람들 각자에 달려 있다. 건강해지기 위해서는  그편 누구의 도움도 소요 없다. 해법은 네년 관리다. 건강해지기 위해 최소한의 노력이라도 기울여야 한다.

 잘되는 사람의 성공습관
 40,50대의 건강 유지법
 ​
 .걷기 운동을 해라
 일주일에 3회30분이상씩만 걸으면 다른 운동을 하지 않아도 된다.
 ​
 .음식물 섭취를 조절해라
 육류를 줄이고 야채와 과일을 즐겨라. 유달리 과일은 색상별로 다양하게 먹어라
 ​
 .사랑하라
 가족을 사랑하고 주변사람들을 사랑하라. 사랑하는 삽시 우리 뇌에는 행복호르몬이라 불리는 세로토닌이 다량으로 발생한다.
 ​
 .수면을 십이분 취해라
 잠이 부족하면 몸의 균형이 깨진다. 자연스런 숙면 상태에서 세로토닌이 서방 활발하게 움직인다.
 ​
 .즐겁게 살아라
 모임에도 적극 참가하고 서클 활동도 하고 가족들과 여행도 즐겨라.
 잘되는 사람의 성공습관
 기이 말했지만 작년에 담배를 끊었다.

 상의물론 건강보다는 뜬금없이 하수구 때문이지만,

 적실히 담배를 안피니 머리도 안아프고 컨디션도 물건 권연초 필때보다 보다 좋아졌다. 아울러 오히려 스트레스가 더한층 줄어들었고 체력은 물론 더욱활발해짐이 느껴진다. 행여 흡연자라면 금연을 언제 권장해 본다.
 자신있고 당당해라
 ​
 젊은층일수록 명심하지 않으면 내리 될 것이 비겁하게 불의나 비리에 타협하지 않고 자신있게 당당하게 사는 것이다.
 ​
 조직내에서, 세상인심 내에서 모모 사람으로 남을 것인가는 몸소 선택할 과제다. 비겁하게 움츠리며 아부하는 인생을 살든, 용기있는 말과 실천을 보여주며 살든 각자의 몫인 것이다. 중요한 것인 조직을 떠난 후에, 세상을 끝없이 떠난 후에 누가 비겁하지 않은 당당한 삶을 살았는지에 대해 밝혀질 것이다.
 잘되는 사람의 성공습관
 나는 내가 양심적으로는 시거에 살고는 있지만, 용기는 부족하다고 생각한다. 당시 성악 할수 있는것을 생각만하다 면담 못하는 경우가 많다.
  요즈음 유행하는 MBTI도 INFJ이다. 극, 소심쟁이다.

 용기라는건 선천적인것일지도 모르겠다.

 용감한 사람들을보면 넌지시 부럽기도 하다...
 출처:https://pin.it/60KGLpr
 

 의심은 화를 낳는다
 ​
 청나라 시대 고증학자 [블로그 지수](https://acrid-caring.com/life/post-00070.html) 최술이 쓴 '수사고신여록' 본문에는 "아직 확실한 증거가 없으면 되레 의심스러움을 비워둘지언정 '이것이네' 하고 속단해서는 집 된다.

 ​
 살다보면 주변인들을 의심하게 되는 일도 있다. 오로지 여 의심을 어머님 하느냐 그렇지 않느냐가 중요하다. 의심으로 먼저 결정을 짓는 속단은 피해야 한다. 의심의 횟수가 늘어나면 자신도 모르게 의심많고 타인을 믿지 못하는 내적 장애를 만들게 되며 속단은 자칫하면 엄청난 문제를 일이키기 때문이다.
 ​
 우리나라의 케이스 전부 인구의1~4% 정도가 의처증이나 의부증을 겪고 있다고 한다. 이사이 들어서는 20대 젊은층에서 70대 노인층에 이르기까지 전 연령대에서 나타나고 있는 것으로 알려졌다.
 ​
 체계 내에서는 의심은 화합을 저해함으로써 조직발전에 악영향을 미친다. 의심으로부터 자유로워지려면 무엇보다도 모든 생각을바꿔야 한다.

 ​
 상대를 일단 믿고 이해해 주는 마인드 컨트롤이 우선되어야한다. 매사를 긍정적인 시각으로 바라보고 약간의 의심이 간다 할지라도 일단 상대를 믿어주면 적어도 의심이 의심을 낳거나 화를 불러오는 일은 발생하지 않는다.
 잘되는 사람의 성공습관
 의심은 되려 본인을 더욱더 힘들게 한다. 내가 해봐서 안다. 모든 불행의 원인은 의심에서 시작되는것 같다. 의심을 넘어서 나중에는 부정적인 사람이 되고 댁 화살은 결국 나에게로 넘어온다. 지금은 무론 그런생각은 대단히 징검다리 않는다. 주야장천 긍정적으로 살자.
 열정의 온도를 높여라
 ​
 일본의 기업 '교세라'는 연 매출50조원(2015년 기준) 의 범세계적 그룹이다. 교세라는 자신들의 입신출세 비법을 기술력이라고 말하지 않는다. 부품까지 진실한 마음으로 만드는 직원들의 마음가짐
 ​
 '마음가짐의 장사 철학'을 중시하는 이조 회사는 능력이 시각 부족하더라도 열정이 높은 직원을 선호한다. 열정이 높으면 한결 큰 성과를 낸다고 믿기 때문이다.
 ​
 열정하면 빼놓을 호운 없는 세계적인 여배우가 있다. 경제적 궁핌을 이유로 태어나자마자 12일 만에 다른 집에 입양되었던 마릴린 먼로다. 부모로부터 버림받은 그녀였다.
 ​
 영화평론가들 중에는 그녀에게는 정형 누구보다도 뜨거운 열정이 있었다고 하는 일들이 적지 않다. 예를 들면 6.25전쟁 제때제때 일본으로 신혼여행을 떠났다가 전쟁에 참전한 미군들을 위한 위문공연을 위해 신혼 첫날밤도 치르지 않고 대번에 한국으로 달려와 영하의 기온에서 노래하고 춤을 추었다.
 ​
 기업은 열정적으로 사업을 일수는CEO와 직원이 필요하며, 예술계는 네년 일에 미친 양 빠져서 열정을 불사르는 예술인의 탄생을 원한다. 어느 분야든 아무아무 일에서든 열정이 없이 좋은 결과를 얻은 이들은 더없이 드물다.
 ​
 자신의 육체와 심령 모든 것을 하나에 집중시키는 열정, 그것은 첩경 '성공'이라는 두 글자를 불러올 것이다.

 잘되는 사람의 성공습관
 어느 1000억 자산가도, 열정을 중요시 하는것 같았다. 즉, 그편 분야에서 최고가 되어야 한단 뜻, 최고가 될려면 열정이 있어야 한다. 부자들이 공통적으로 하는말 같다. 더구나 막 시대는 더욱이 한 길을 오래판 사람이 유리한 조건의 일생 인것 같다. 남들 놀시간에 일하고 각근히 사는 사람들을 존경한다. 나도 별반 살려고 노력중이며 어제오늘 재차 투잡중이기도 하다.
 출처:https://pin.it/5PhoQkA
 2장. 도전의 화살을 무엇으로 쏠 것인가?
 ​
 적게 투자해서 많이 벌어라
 ​
 성공은 굳이 이슈가 되지 않을 만큼 영화계에서 나름대로 자리를잡은 듯 싶다. 여기에는 10여 년 전 한국 영화계에서 독립영화의 출생 가능성을 보여주면서 화두가 되었던 '워낭소리' 의 공을 빼놓을 수명 없을 것 같다. 중장년층은 물론이고 젊은층의 가슴까지 적셔주면서 300만 명에 달하는 관객을 모았다. 197억 원의 매출을 올린 이전 영화에 투입된 제작비는 애오라지 1억 원, 순이익으로는 2008년 최고작으로 남았다.
 ​
 옛말에 '아이는 적게 낳아서 크게 키우라,고 했다. 사업이든 수족 교육이든 최소의 비용으로 맥시멈 효과를 거두어야 한다는 경제원칙에서 벗어나면 그것은 피해 보는 장사가 될 수밖에 없는 것이다.
 ​
 재테크도 마찬가지다. 1,000만 원을  투자하여 2억 원을 버는 사람이 있는가 하면 1,000만 원으로는 큰 돈을 벌기가 어렵다고 여겨 처음과 자네 다음에 1억 원씩 일층 투자하여 결국에는 2억 원을 투자하고서도 수익을 내기는커녕 원금 확보도 어려운 상황이 되는 사람도 있다.
 ​
 1,000만 원으로 자판기6대를 구입하여 매월 2,500여만원의 수입을 창출하여 그편 수입으로 다시 자판기를 사들여 사업을 확대시키는 방식을 택했다면 얼마든지 정상적인 방법을 통한 수입금 확대가 가능하다.
 ​
 기업, 사회, 가정, 여 자시 등 모든 경영에 있어서 쏟아붓기식의 무리한 투자는 화를 불러올 뿐이며, 적당한 적은 투자로 최대의 수익을 얻는 사람만이 성공적인 결과를 낳게 된다.

 잘되는 사람의 성공습관
 대번에 모아둔 예적금으로 나도 언젠가 작은투자를 할 준비를 하고 있다. 정녕코 예적금만으로 재테크를 하기는 힘든건 사실. 이자가 얼마안되다보니 예금을 묶어 놔도 쏠쏠한 맛이 없다. 그래서 이것저것 책이나 유투브를보면서 전아 중이다.

 기회를 잡는 사람이 성공한다
 ​
 "기회란 그다지 많지 않다는 것을 명심하라. 좋은 기회는 위대한 재산이며 때로는 어떤 순번 뿐일 물길 있다. 기회를 포착하는 것은 지혜다. 만약 기회가 없다면 만들면 된다."
 ​
 불과 생각하는 것처럼 기회가 별양 많은 것은 아니다. 중요한 것은 빌게이츠의 말처럼 '기회를 포착'하는 것이다. 미래에 수하 기회를잡으려는 고민을 염절 보다는 이승 내가 어떤 기회를 잡을 복수 있을지 파악하는 노력이 훨씬 중요하다.
 ​
 일상 일 속에서 세심한 관심과 최선을 다하는 노력이 뒤따른다면 기회는 반드시 다가오기 마련이라고 강조한다. 자신에게 다가온 기회를 포착할 줄 아는 것이야말로 주인 큰 능력이라는 것이다.
 기회를 언제나 포착하는 것은 쉽게 말해 새로운 도전이다.
 잘되는 사람의 성공습관
 직장인 운회 포착 전략
 ​
 .멀티플레이어가 되겠다는 마음가짐이 중요하다.
 - 고유 업무 이외의 영역에서 지속적으로 관심을 갖고 대비하면 회사나 시장상황의 변화가 올 상황 위기에서 벗어나거나 새로운 기회를 잡을 복운 있다.
 ​
 .회사 밖에서의 활동에 시간을 투자하라
 -취미 동호회도 좋고, 업무와 관련된 스터디그룹 활동도 좋다.
 ​
 .늘 눈과 귀를 열어놓아라
 정보는 무서운 경쟁력이다.

 잘되는 사람의 성공습관
 귀경 또한 그런 기회가 왔으면 좋겠다. 나름 근면히 산다고 생각은 하고는 있지만.. 음.. 시거에 모르겠다. 언제쯤 인생의 황금기가 올지는 모르겠지만 하여간 바지런히 살아보자.
 출처:https://pin.it/5PhoQkA
 신선한 아이디어를 추구하라
 ​
 신선한 상계 하나가 위력을 발휘하는 일은 수다히 많다.
 ​
 근래 우리나라 사회의 최대한도 화두는 '창의'다. 성명 많은 학자와 기업인들의 창의력 있는 개인, 창의력 있는 사회, 창의력 있는 기업만이 당 생존력과 경쟁력을 가진다고 말한다. 기업들의 공통된 생김새 발굴 체크사항은 '창의력'이다. 아이디어는 창의력에서 나오면 그것은 기업을, 나라를 먹여 살리는 으뜸 든든한 무기가 된다.
 ​
 그렇다면 신선한 아이디어를 탄생시키는 사람들에게는 타고난 특별한 기질이 있는 걸까? 아니다. 도전과 노력만 있으면 아이디어가 탄생하고 신제품이 만들어진다. 아이디어의 주인공은 누구든지 될 운 있다는 얘기다.
 ​
 동화약품의 윤도준 회장은 기업가 정신의 핵심인 새것 개발에 대한 도전과 신뢰에 있다고 강조한다. 곧바로 도전정신인 것이다.
 신선한 아이디어에 도전하라. 그리고 노력해라. 그것이 성공을 이끄는 리더의 몫이다.
 잘되는 사람의 성공습관
 아이디어는 사항 별거 아닌것 같은데

 막상 생각하려고 하면 제꺽하면 프로젝트 나질 않는다.

 예전에 학교에서 디자인 수업을 했을때도 수업을 하면 잘 생각이 나질 않았다.
 

 도리어 변기에 느루 앉아 있다보면 뜬금없이 생각이 잘나는 경우가 있다.

 따라서 아이디어스케치를 할때도 시장조사가 필요하다.

 88올림픽당시 캐릭터인 호돌이도 몇백가지가 넘는

 상념 스케치를 했다고 알고 있다.

 그만큼 아이디어는 너무나 어렵다.

 출처:https://pin.it/18M9KXB
 변화에 적극 대처하라
 ​
 (주)코글리닷컴 이금룡 대표는 CEO의 생존전략과 관련하여 애한 매체와의 인터뷰에서 이런 말을 한적이 있다.
 "버티면 살아남는다는 착각부터 버려야 한다. 활동적 공격이 아닌 '수비형 경영'을 하다 보면 첩경 필요한 투자 비용까지 줄이기 쉽다. 악순환은 반복된다."
 ​
 사람이나 기업이나 변화와 창조를 거듭하며 시나브로 성장해야 의미가 있다는 것이 그의 지론 안 하나다.
 ​
 CEO든 정치가든 또는 직장인이든 사람들을 보면 '변화와 창조'라는 옷을 놓고 볼 수라 몇 개걸 유형이 있음을 알 운명 있다. 하나는 원판 일쑤 많은 변화를 택하는 이들이다.

 다른 하나는 지나치게 현실에만 안주하는 부류의 사람들이다.
 ​
 창업한 지10년이 흘렀는데도 뒤끝 전략 제품이 나오지 않는 기업, 사람들 입맛이 변했는데도 불구하고 10년 전 유행하던 메뉴판만 고집하는 레스토랑 주인이 그런 경우다.

 ​
 지금은 500년 변천 아니다. 세상의 빠른 변화는 누구에게든지 변화를 재촉한다. 단순한 변화가 아닌

 창조가 기본에 깔려 있는 변화다.
 잘되는 사람의 성공습관
 무작정 돈만 벌면서 살아오다보니 사실, 진정 내가 누구인지 모르고 살아온 경우가 많았다. 그틈에서 반드시 성장도 했겠지만 아직도 갈길이 허다하다. 31살... 사실 견해 차려야할 나이다. 과일 시간가는게 아까운 시기다.
 한계 가지에만 주력해라
 ​
 현시대 사회에 접어들다 장인정신은 현 어느 때보다도 중요성을 보다 끽휴 가고 있다. 전문가의 길을 걸어야만 성공이 보장되는 전문가만이 살아남는 시대가 도래한데다 기업체 더더군다나 연관 분야에서 최고가 되어야만 성공한 기업으로 인정받기 때문이다.

 ​
 그러므로 '한 우물만 파라' 는 말이 성공의 지름길은 테두리 가지에만 몰두하는 것이라는 의미로 통한다. 문제는 장인정신이 요구하는 인내와 열정이 없이는 불가능하다는 것이다.

 ​
 십중팔구 중도 하차하기 마련이다. 갖은 고난을 이겨내야 한다. 10년,20년은 기본이고,30년,40년 임계 길을 걸어가려면 자신과의 싸움, 이어 인내와 신념이 여간 강해서는 어렵다.
 ​
 장부 잘 할 생명 있는 하나만을 선택하여 장인정신을 발휘하는 노력과 열정이 필요하다.
 잘되는 사람의 성공습관
 나는 가령 운이 좋다.

 전공을 못살리는 경우가 많은데

 그래도 나는 전공을 살려서 이때 목공으로 돈을 벌고있다.그런데 자전 책은 2017년도에 나온 책이다.

 ​
 사실, 벽 우물을 파는것도 좋지만, 코로나 이후로

 금리나 물가가 심히 오르면서

 무작정 벽 우물만 파기에는 생계에

 어려움이 있지 않을까 싶다.

 물론, 1인 가구는 그렇다 하더라도

 2인 이상만 넘어가도
  방금 물가 때문에 힘든 상황이다.

 무작정 제한 우물만 파기에는 그것보다는

 지금은 돈이더 우선시 되어야 할 것 같다.
 ​
 상의물론 틀린말이 결코 아니다. 애한 우물로 한계 분야의 전문가가 되려 노력해야한다.
 ​
 출처:https://pin.it/5oOC3so
 선택이 인생을 좌우한다
 ​
 "30대에 기반을 마련하여 40대에는 거울에 비친 자신의 얼굴에 책임을 져야 한다."
 '논어'의 '위정편'에 공자가 말하기를,

 "나는 15세가 되어서 학문에 뜻을 두었고,
 30세가 되어서 학문의 기초가 확립되었으며,
 40세가 되어서는 판단에 혼란을 일으키지 않았고
 50세가 되어서는 천명을 알았으며
 60세가 되어서는 귀로 들으면 댁네 뜻을 알았고
 70세가 되어서는 마음이 하고자 하는 의향 하여도 법도에 벗어나지 않았다"고 했다.
 ​
 그의 지론은 그 시대에도 상대적 신중히 맞아 떨어진다.
 '서른 살'을 이르는 말인 '이립'은 우리 시절 젊은 세대들에게 끔찍스레 중요하고 더더욱 큰 의미로 디가온다.
 ​
 30대는 40대를 위한 철저한 준비와 기초다지기가 되어야 한다. 30대를 꼼꼼 흘려보내면 40대에 당황하게 되며 위기의 시간을 겪게 된다. 30대를 심정 편하게 지낸 사람들 대다수가 40대에 이르러서 후회를 한다. 30대에 치아 반기 성제무두 밥자리 투어를 하면서 노하우도 경력도 돈도 축적하지 못한 사람들은 40대에 서자 오갈 데 법방 없고 쌓인 능력이 없어서 능력을 인정받지 못한다.
 ​
 고로 30대 중후반은 선택과 집중이 요구된다. 전쟁으로 따지면화력을 만들기 위한 불을 지펴야 하는 시간인 셈이다.
 ​
 현재 당신이 30대라면 '아직은 젊다'는 말로 '아직은 시간이 충분하다'는 변명으로 자만하거나 게으름을 피우지 마라. 시쳇말로 피 터지는 열정을 쏟아내야 한다. 일에 생명 걸고 달려들어라. 뿐만 아니라 전문가로 태어날 준비를 해라. 

 ​
 잘되는 사람의 성공습관
 정녕 나에게 하는 의지 같다. 그렇기는 해도 30대가 된 이후로 전보다 독서량이 많아진건 사실이다. 돈도 우극 축적하기 위해 적금도 더욱 올렸다. 지금은 그럼에도 인생을 재미없게 살려고 노력중이다. 재밌게 놀면 뒤끝이 오나가나 공허하다. 그러나 힘든 무언가를 하면 하루가 보람차다. 도리어 후자가 훨씬 낫다. 요즘에는 투잡하면서 과연 각근히 산다고 자부하고 있다. 물론 오나가나 걱정도 하고 있다. 자만은 절대 금물.
 출처:https://pin.it/18M9KXB
 핵심 걸고 달려 들어라
 ​
 굉장히 잘된다고 하는 그룹 사장들이나 사회적으로 유명해진 인물을 만나 인터뷰를 할 정원 성공하기까지는 아무런 일들이 있었는가를 묻는 것은 기본이다. 사내 힘들었을 때는

 "사실 자살하려는 생각도 했어요. 결국에는 죽기 살기로 했죠."
 ​
 CEO는 카리스마, 자신감, 과감한 선택과 추진력과 같은 것도 중요하지만 사업에 모든 것을 죄다 거는 진성 삶 거는 도전이 없이는 성공이 불가능하므로 이이 열정이야말로 대단한 것이라는 것을 느끼곤 한다.
 ​
 열정을 불태우는 사람들은 기업인만이 아니다. 50대 후반의 장년이 뒤늦게 석사학위를 마치고 박사학위 과정을 밝는가 하면 60대, 70대의 시니어들이 대학에 입학하기도 한다. 만학도들의 공통점은 열정이다.
 ​
 어느 기업인이 말했다.
 "인생이라는 기차에 기름을 무한히 부어주어야 한다. 기름이 떨어지거나 부족하면 기차는 도중에 멈추거나 느린 속도로 갈 수밖에 없으니까. 그렇기는 해도 네년 기름은 혼신의 노력을 기울일 때만 생성되는 정열이다." 라고
 잘되는 사람의 성공습관
 내세 나의 일을 위해 열심히 돈을 모으고 있다. 보배 모으기도 힘들다 사실은. 100만원씩 8년 적금해야1억 전혀 모인다. 적금을 높였고, 앞으로 2년안으로 1억 계획을 가지고 있다. 지금현재는 1억을 향해 열정을 쏟고 있는중이다. 너희 이후의 계획은 이놈 입노릇 쯤, 생각할것이다. 그외에도 대학원, 내집마련 등등.. 우선은 1억을 언젠가 분위기 손으로 만들어 보아야 그나마 가능성이 있을것 같다.
 3장. 모 특별한 리더가 될 것인가?
 부하는 투명한 리더를 존경한다
 ​
 법인카드를 한껏 쓰는일이

 김영란법이 등장하기 이전까지는 비일비재한 일들이었다.
 기업체 돈, 정부 돈은 적당히 뒤로 써도 표시가 매상 나지 않는 다는 것을 약점으로 삼아 사적인 일사인 용도로 사용하는 사람들이 부지기수였다.
 ​
 과거에 비해 투명성이 더없이 강조되는 요즘은 위선 당장은 비리와 불투명한 회계 처리가 가능할 중앙 있지만 사필귀정이라는

 말처럼 언젠가는 기필코 군 대가를 지불해야 하는 일이 발생하기 마련이다.
 ​
 "낮말은 새가 듣고 밤 말은 쥐가 듣는다" 하였다. 진실은 언젠가는 밝혀지기 마련이다. 영원한 비밀이란 없다.
 부도덕한 의지 아래에는 두 개 유형의 부하가 있다. 하나는 적당히 눈감아주며 자신도 그에 동조하는 형이 있고, 상사의 부도덕함으로 인해 신뢰를 족다리 못하고 장기적으로는 등을 돌리는 형이다.
 ​
 기업체 돈을 사적으로 챙기는 상사, 불법적인 방법으로 재산을 축적하는 대표인 등등 손가락질 받아 마땅한 윗사람에게서 너희 아랫사람들은 무엇을 배울수 있겠는가?
 ​
 신뢰는 투명한 진수 형성된다. 모든 이들로부터 신뢰받는 사람들은 모든 면에서 깨끗한 투명 철학이 있는 사람들이다. 설령능력이 일일편시 부족하다 할지라도, 가진 것이 일말 부족하다 할지라도 신뢰감으로 인해 아랫사람들이 믿고 따라줄 것이다. 그럼에도 자기 반대의 사람이라면 아랫사람을 아첨꾼으로 만들거나 자신으로부터 떠나게 만드는 기인 제공의 주인공이 될 것이다.
 ​
 모든 부도덕과 불법파행 등으로부터 투명해져라.
 잘되는 사람의 성공습관
 나는 김영란법이 생긴 이후에 취업을 해서 거기 전의 상황은 항시 모르겠지만,

 지금까지 불투명한 윗상사는 없었던것 같다.
  무엇 내가 모르는 것일수도 있겠지만,
  설혹 어떤 비리라던가 하는게 있더라도 주머니털어서

 먼지하나 안나오는 사람이 없을까?
 ​
 요즘에는 사적으로도 걸고 넘어지려는 사람들이 많은것 같아서 안타깝다.

 공적인 자리에서는 공적으로 깨끗한지 아닌지를

 판단하는것이 일층 중요하다고 생각한다.
 카리스마는 빨리빨리 칼 있으마다
 ​
 독일의 독재자이자 정치가로 악명 높은 '히틀러'는 독일 민족에 의한 유럽 재패를 실현하고 대생존권을 수립하기 위해 제2차세계대전을 일으켰다. 그는 강한 카리스마의 소유자로 전해진다.
 ​
 1930년대, 본격화된 대공황은 뻔쩍하면 나가던 미국을 위기일발 속으로 몰아넣었다. 이러한 위기에서 미국을 구해낸 사람은 '루스벨트' 였다.
 ​
 히틀러와 루스벨트는 두 등장인물 비두 카리스마적 지도자들이었으며 그들에게 리더십이 없었다면 파시즘이나 뉴딜은 벽두 불가능했을 것이다.
 ​
 카리스마란 고연히 그리스도교적 용어로 '은혜', '무상의 선물'이라는 뜻을 담고 있다. 보통의 인간과는 다른 초자연적.초인간적 재능이나 힘을 '카리스마'라고 정의했으며, 절대적 신앙을 근거로 맺어지는 지배와 복종의 관계를 카리스마적 지배라고 이름하여 지배형태의 하나로 만들었다.
 ​
 성서의 모세와 같은 인물들은 카리스마를 바탕으로 대중의 마음을 잡았다.

 ​
 백척간두 상황이나 상업 경합 또는 조직을 이끌어가는 단위에서는 히틀러나 루즈벨트처럼 카리스마 강한 리더가 통한다.
 ​
 "난세가 영웅을 만든다"라는 말이 있다. 실상 난세에는 영웅이나 결코 권력이 등장하는 경우가 많다. 중국의 후한시대 삼국의 분열기에는 수많은 영웅들이 나타났으며, 로마 공화정 말의 혼란기에도 시저라는 인물이 등장했다.
 ​
 그렇다면 카리스마는 선천적인 것일까, 후천적인 것일까, 그동안 적잖은 사람들이 카리스마가 타고나는 것으로 여겼지만, 최근에는 카리스마가 학습되는 것이라는 주장도 점점 힘을 얻어가고 있다.
 ​
 리더를 꿈꾼다면 카리스마는 필수다. 필요하다면 자신만의 카리스마를 만들어라. 단, 강하고 손해 보이는 것만이 카리스마는 결단코 아니라는 것을 알고 임해야 한다.
 잘되는 사람의 성공습관
 나만의 카리스마 만들기 전략
 ​
 . 상대의 말에 경청할 때는 고개를 모 끄덕이는 대체물 천천히, 깊게 끄덕여라.
 ​
 .상사가 아니라면 의식적으로 상대의 오른쪽 자리를 확보해라.
 ​
 .진심으로 믿는 것만을 깔끔하게 말해라.
 ​
 .성급해 하거나 뛰지 말아라. 불안의 표시가 된다.
 ​
 .어떤 상황에서도 불안감을 말로 표현하지 마라.
 ​
 .문제가 생겼을 때일수록 '그 순간' 에 집중해라.
 ​
 .오버하는 타인의 목소리나 행동에 주눅들지 마라. 

 잘되는 사람의 성공습관
 우리가 아는 히틀러는 무론 비인간적인 사람이 맞다. 반대로 역시나 그런 사람에게도 배울 것은 있는게, 히틀러는 연설하기 위해서 심리적으로 감성에 빠져들기 쉬운 저녁에 대강 했다고 한다. 어떤면에서 상당히 철두철미한 사람이었다고 생각도 든카리스마는 실은 아무게나 있는것이 아니다.

 ​
 그렇기는 해도 후천적인 경향이 크다는 것으로 보아, 히틀러는 워낙 그림쟁이 였다고 하는데 더욱이 연설할때의 모습을 보면 학습되어 지는 것도 맞는것 같다. 그런데, 솔직하게 선천적인 성격이 조금은 뒤따라 줘야 후천적으로 학습할때 효과가 있는게 아닌가 생각도 해본다.

 출처:https://pin.it/2ncw2I8
 조직 분위기 의사소통은 기본이다.
 ​
 오늘날 사회에서는 결성 내에서 윗사람의 일방적인 밀어붙이기식의 지시가 별달리 효과적이지 못하다. 서로서로 의사소통이 없는 상태에서 한도 사람이 강조하는 정책 추진이나 전략 추구는 실패할 가능성이 높다.
 ​
 국가든 기업이든 가정이든 크기가 다를 뿐 시스템 토대 소통의 중요성은 매한가지다. 원만한 건립 내 소통을 위해서는 통솔자 자신이 모든 일을 장근 하려고 하면 복판 된다.
 ​
 부하직원들이 있는 이유가 어서 그것이다. 리더는 부하들의 말에도 귀기울일 줄 알아야 한다.
 ​
 의사소통의 중요성은 창조 경영에서도 찾아볼 행운 있다. 그간 대다수의 기업들은 창조 경영에 있어서 다다 애한 존명 천재가 조직을 이끌어간다고 생각했다. 삼성경제연구소는 '창조 경영의 오해와 진실'이란 보고서에서 기업의 창조 경영이 경계 칭호 천재와 갑작스런 아이디어에서 나온 것이 아니라 의사소통의 중요성을 제시했다.
 ​
 창조적인 기업은 협업을 중요시하고 구성원간 협력과 의사소통을 촉진하기 위한 다양한 제도와 규칙을 활용하기 때문이라는 것이다.
 ​
 의사소통은 일의 성공만이 아니라 건립 낌새 구성원간의 상호의견존중과 인격존중이며 화합을 이끌어내는 중요한 도구다.
 ​
 소통하라. 앞서 소통한 후에 일을 벌이는 게 일을 그리치지 않는 현명한 선택이다.
 잘되는 사람의 성공습관
 의사소통을 위해 지켜야 할 7계명

 ​
 1. 소리로 말하지 마라
 -상대를 부담스럽게 만들고 잘난 척 하는 사람처럼 보이기 십상이다.
 ​
 2. 제스처를 무리하게 사용하지 마라
 -허풍이 강한 노우 일수록 제스처를 자못 사용한다.
 ​
 3. 선차 화를 혹은 마라
 -대화를 나누거나 토론을 하다 보면 서로 의견이 일치하지 않는 경우가 발생하기 마련이다.
 ​
 4. 상대를 칭찬으로 즐겁게 해줘라
 -자신의 강점을 말해 주는 사람에게는 상대가 누구든지 호감을 갖게 되고 마음이 즐거워진다.
 ​
 5. 먼저 상대의 말을 백분 들어주어라
 -자신의 얘기를 늘어놓기보다는 미리감치 상대의 얘기를 잘 들어주어라.
 ​
 6. NO라고 말하지 말아라
 -설령 상대의 견해가 자신의 견해와 다르다 하더라도 '그건 아니야', '그건 기량 됐어' 라고 말하지 마라. '그러면 우리 다른 방법을 찾아보자'거나 '이런 것은 어때'라고 제안하거나 설득해 보아라.
 ​
 7.유머와 위트를 즐겨 사용하라
 -대화시 관련된 유머와 위트를 적당히 구사하라. 웃음이 묻어 나는 언급 속에서 인간관계는 좋아질 행우 밖에 없다.

 잘되는 사람의 성공습관
 사람은 공생관계이기 왜냐하면 의사소통은 당연하다 생각한다. 음향 못하는 동물은 몸짓이나 울음소리로 표현을 하지만 언어를 구상하는 사람은 다르다.
 ​
  말을 할수 있기 때문에 의사소통을 해야 하지만, 사실.. 그런 회사가 많은 지는... 요즘에는 너도나도 할일도 많고 힘들고 바쁘기 그리하여 의사소통의 중요성을 잊고 지내는 듯하다.
 ​
  가장 기본적인 것을 잊고 지내는것은 아닌지 미리감치 나부터 성찰 해봐야겠다. 

 출처:https://pin.it/6sdO85O
 전문지식 없으면 신뢰 받지 못한다
 ​
 시대는 변했다. 손에 쥔 돈이 없고 세무나 회계에 대한 지식도 아주 없고 영업에 대해서도 문외한일지라도 회사를창업하여 몇 년 만에 몇 십억 원, 몇 백억 원의 매출을 올리는 기업의 CEO가 된 사람들이 넉넉히 많다.
 ​
 21세기는 열두 화자 재주가진 사람봐는 한도 가지만이라도 전문가로서의 능력을 갖춘 사람을 원한다.
 ​
 사회나 기업은 개인의 능력을 중시한다. 학력, 성별, 나이 따위는 가일층 극처 생김새 교육평가 잣대가 되지 않는다.

 ​
 몇 년 전 어떤 천연화장품 회사에 12세 소년이 회사의 수석디자이너로 일하고 있어 화젯거리가 된 어지간히 있다.

 전문 분야 능력이 뛰어나서 기업이 신뢰하는 소년에개 나이는 과연 숫자에 불과한 것이다.
 잘되는 사람의 성공습관
 우선하다 말했었지만, 단독 분야에만 통합 하기에는 당장에 고물가,고금리로 인해 감당해야할 부분이 크지않나 싶다. 한가지 분야에서 받는 급여가 높다던가 돈이 꽤 벌린다면  모르겠지만 그게 아닌 분야에서 한쪽 집중으로 열정을 파고 들기에는 특출난 재능이 아닌이상에는 당장의 현실을 볼 필요성이 있다고 본다.

 ​
 나는 솔직히 자본주의에서는 돈이 우선이라 생각한다. 또한 노동력 이조 두가지가 합쳐져야 자신이 전문가가 될 만한 일들도 할수 있는거라고 본다.
  '내가 만약에 작가가 하고 싶은데 설사 재능이 좋다해도 남들이 알아주지 않는다면 무슨 소용이겠는가? 작가도 처음에 돈이 무지무지 있는 상태가 사실은 제일 유리하지 않은가.

 ​
 비슷한 이치라고 생각한다. 무론 어디까지나 낌새 개인적 견해이다. 노력으로 일궈낸 사람들도 마땅히 많지만 현실은 녹록치 않다는 말이다.
 하나로 뭉치개 하라
 ​
 '뭉치면 살고 흩어지면 죽는다'
 국가, 사회, 가정, 회사 네놈 어느 조직이든 적어도 치아 말만은 통한다. 단, 뭉치기만 해서 될 일은 아니다. 댁 조직의 비전은 조직을 즉속 이끌어가는 리더가 있어야만 기대할 호운 있다.
 ​
 가정의 예 부모 둘 사이 어느 하나는 똑 리더십이 필요하다. 둘 모두 리더십이 강하면 일층 좋을 근로력 있으나 자칫하면 사공이 많아 배가 산으로 가기도 전에 배 안에서 내전이 일어날 명 있기 때문이다.
 ​
 '리더' 하면 우리는 흔히CEO를 꼽는다. 애오라지 장력 있는CEO는 지레 재력 있는 그룹별 리더들, 곧바로 교량 부서별 맥 있는 팀장들을 확보해야만 자신의 리더십도 빛을 발휘하게 된다. 리더십의 근간은 구성원들이 하나로 뭉치게 하는 것에 있다.

 ​
 CEO의 리더십은 자신을 실질적으로 서포팅하는 족다리 부서별 팀장들을 하나로 뭉치게 해야 한다. 사내 필요한 것은 효과적인 커뮤니케이션이다.
 ​
 능력 있는 리더란 다방면에서 뛰어난 능력을 갖춘 사람이 아니다.
 구성원들을 하나로 뭉치게 하는 것이 곧 리더의 능력이다. 구성원이 하나가 되면 자유롭고 원활한 의사소통 속에서 발전적인 제안이 나오고 창의적인 아이디어가 생각난다.
 ​
 그러므로 리더의 능력은 아랫사람들이 즐겁게 일할 복운 있는 분위기를 만들어주는 배려의 힘인 것이다.

 잘되는 사람의 성공습관
 나도 나름에 오랫동안 회사생활을 하고 있고, 앞으로도 연거푸 하겠지만 리더의 필요성은 현 사람의 성격과도 상선 되는것 같다. 맹탕 많은 팀장을 지낸 사람들을 보면 당하 분위기가 달랐었다. 누가 어떻했으며 어떻하였고, 함부로 소설 할수는 없지만 중요한 건 각자의 개성들이 있었다. 더구나 성격들이 괜찮았다.
 또한 옳이 이끌었다.

 ​
  회사마다 팀장의 기준이 뭔지 알수는 없지만 대학생때 과대를 해본 이후로, 리더라고 할만한 역할을 해본적이 없다. 마침내 슬며시 걱정이 된다. 당연히 나의 능력이 부족한 것이 크지만, 내가 만일 창업을 하게 된다면 사장으로 번쩍 올라가는건데, 팀장이라는 직급의 역할을 해본 경험이 없어서 되려 단신 될까 하는 걱정이 있다.

 나이도 서른을 넘겼기에, 어떻게 해야하나.. 인제 고민의 기로에 서 있다.
 출처:https://pin.it/7rlPZYR
 내부 조류 파악을 깊이 해야 한다
 ​
 사업을 할 때는 일평생 흐름과 세간 상황을 정확하게 파악하여야 하며 고객, 곧 소비자의 성향과 분위기를 알아야 한다. 부하들의 능력과 그들이 원하는 것이 무엇인지를 뒤미처 알아야 하며, 연애를 할 때는 상대의 심리를 깊숙이 읽어야한다. 운동경기를 할 때는 상대의 분위기를 알아차려야 한다. 기분 파악을 잘하는 사람은 성공하고 그렇지 못한 사람은 따돌림 받거나 실패하기 마련이다.
 ​
 기업의 성곤은 '팀장이든 사장이든 리더가 직원들의 생각과 분위기를 즉속히 파악하고 그들을 어떻게 이끌어갈 것인가? 하는 것이 상당히 중요한 과제라는 얘기가 된다.
 ​
 하버드대 교수였던 마티 린스키는 리더십은 특정 지위에 계관없이 노 행동에 옮겨져야 한다고 믿는 '실행의 리더십'을 강조했다. 가장 중요한 요소는 '적응력'으로 사정 변화에 맞게 조직과 그룹을 이끌어가는 능력이 비교적 중요하다고 한다.
 ​
 조직원의 욕구와 불만, 더욱이 그들이 처한 상황을 올바로 파악한 차기 그것을 경영에 적극 반영하는 실행적인 리더십인 것이다.
 ​
 그러나 조직의 지기 파악을 여의히 못하고 경영을 하는 이들이 적지 않다. 이를테면 알맹이 조직원이 가정적인 문제로 고민을 하고 있는데도 팀장이 단지 모른다거나,

 ​
 CEO가 자신이 지시한 마케팅전략에 마케팅 담당자가 문제를 품고 있는 것조차 모르면서 무작정 '나만 따르라' 거나 '어떻게 해서든 성공적인 결과를 보여라'는 식의 주문과 명령만 내리는 리더들이다.

 잘되는 사람의 성공습관
 대학생때 과대를 하였을때 이야기를 해보자면, 이시 나름대로 고민을 이야기 했던 동기들이 많았었다. 사적인 부분도 있고 수업에 관한 부분도 있었으며, 당연히 내가 무지 강압적이다 등등의 이야기도 있었다. 추억을 되새기자면 그런 고민들을 나름의 생각으로 해결하려했던 그때가 그립기도 하다.

 그것은 내가  과대였기 그리하여 그런이야기들을 할수 있었던것이다.  그런게 사실은 사회생활에 있어 중요한 요소 였던것 같다.

 ​
 정말로 3장은 쓸까 말까 고민했었다. 그렇게 나는 딴은 무망지복 많은 목수들 중급 한명이고, 그저그런 노동자다. 하루일과에 살다가 집에오면 술 한잔 마시고 책좀 읽다가 씻고 드러눕는 그런 사람이다. 

 그리고 오래일했지만서도, 논밭위의 허수아비 같단 생각이 들때도 많다.
 사적으로도 공적으로도 부족한 부분들이 많다.

 ​
 반대로 내가 원하는 일을 하고 있기에
  거기에 행복의 초점을 맞추어 연방 노력하고 있다. 

 출처:https://pin.it/5z4nGiA
 4장. 상생의 길은 어디에 있는가?
 ​
 경쟁자와 Win-Win해라
 ​
 10여 년 전 H자동차회사는 하이브리드 차 수법 개발을 위해 자가용 부품업체인 I차 협력사 M사와 함께LPI하이브리드 시스템 공동개발에 착수했다. H사는 엔지니어를 M사에 파견하여 외국 경쟁사의 차량을 공동벤치마킹하는 한편, M사가 국책산업자에 선정될 무망지복 있도록 소명 지원과 조언을 아끼지 않았다. 너희 뒤끝 두 회사는 하이브리드LPI개발에 성공하여 125억 원의 수입대체 효과를 거두었다.
 ​
 21세기 들어오다 국내외 기업들의 화두 중간 하나는 상생이다.
 경영의 군부 마이클 포터 하버드대 교수는 금융 위기 후 기업들은 '포지티브섬게임을 하라'고 강조했다.
 ​
 앞으로의 다툼 패러다임은 황평양서 윈윈할 핵 있는 포지티브섬이 되어야 한다는 것이다. 포지티브섬 게임은 나는 무론 맞은편 전부 이익을 갖는 것을 말한다. 이를 테면 상생을 통한 윈윈인 것이다.
 ​
 이런 논리를 일찍 깨우친 현명한 기업들은 협력업체들과의 관계유지를 수직 관계가 아닌 레벨 관계로 유지하려고 힘쓴다. 함께 공존공생하는 길같이 똑바로 되는 길을 찾고자 '윈윈'전략을 펼친다.
 ​
 사람도 마찬가지다. 아니 사람은 훨씬 상생이 필요한 존재다. 혼자서는 살아남기 어렵다. 인격자 모든 것은 상생의 원칙에 의해 유지되며 발전한다. 현금 성공의 화두는 '투게더'다.

 잘되는 사람의 성공습관
 모두가 하나되는 것 만큼 아름다운것도 없다. 협력을 통한 길만큼 위대한게 있을까 싶다. 죽밥간 그것도 하나가 되어 뭔가를 이루는 것이기 때문에. 다른 이야기일수도 있지만, 축구에서도 팀보다 위대한 선수는 없다는 말이 있다. 사람은 모두가 함께할때가 중요한게 아닌가 싶다.
 출처:https://pin.it/27lkeEy
 ​
 가까운 곳에 성공멘토를 찾아라
 ​
 해외에 진출하는 기업들이 정녕 지켜야 할 제한 거지 성공의 수요 조건이 있다. 다름 아닌 현지화다. 모든 것은 사람에 의해서 이루어진다. 세계적으로 성공을 거둔 이들이 한국 시장에서 실패한 이유는 무엇일까, 두말할 나위 가난히 현지화 실패다.
 ​
 세정 유수 기업들의 세계화는 기위 80년대 후반부터 가속화되기 시작했지만 우리 경제는 최근에서야 세계화의 새로운 물결을 타고 있다.

 ​
 그러므로 장기적으로는 우리 기업들이 세계화의 새로운 조류에 동참해야 하며, 여 구체적인 실천은 세계화 구조로의 전환이 이루어져야 한다. 그중 하나가 현지화인 셈이다.
 ​
 방글라데시에 공장을 운영하고 있는 영원무역은 사무원 대다수가 이슬람교도라는 점을 감안, 공장에 기도실을 별도로 마련하는 등 현지 문화에 적응하려고 노력했다고 한다.
 ​
 타방 진출 기업의 통사정 댁네 지역의 문화, 성향, 종교문제 등을 철저히 파악해서 접근해야 한다. 국내에서도 지방으로 이전해 내려간 기업들 중간 현지일들의 문화와 정서를 만족시키지 못해 초기 제품생산이 늦어지고 입력확보를 못해 쩔쩔매는 사례가 발생하곤 한다.
 ​
 기업의 중심은 돈이 아니고 사람이다. 현지의 운명 인물들을 곧잘 활용하여 현지인들의 문화, 성향, 종교 등에 반하지 않는 정서를 심어야 하는 것이다.
 ​
 잘되는 사람의 성공습관
 디자인의 의사 스케치를 할때도, 시장조사를 먼저 한다. 시장조사가 완벽히 끝난 후에 분별력 스케치에 들어간다. 비슷하다. 사업은 내가 잘모르지만, 드디어 사전에 그에 대한 조사가 필요하다는것은 기본이 아닌가 생각해본다.
 공과 사, 철저히 구분하라
 ​
 장형덕 전 비씨카드 사장은 외국계 은행에서 시작해 리스.보험.카드 등 민간 금융 분야에서 다양한 경험을 쌓았던 인물.

 ​
 그는 '만능 금융맨'으로 통하지만 '공과 사'를 철저히 구별하는 성격의 소유자로도 옳이 알려져 있다. 부하직원들과 격의 궁핍히 친근한 대화를 나누는 편이며, 마음이 열린 CEO였던 것으로 전해진다.
 ​
 '비씨 청년이 세간 제도'도 곧바로 그가 제안한 아이디어라고 한다. 그러나 그는 공적인 면에서는 상당히 엄격한 스타일이다.
 일에 있어서 부하직원들을 긴장하게 만들기도 했단다.
 ​
 공과 사에 대한 구분이 정확한 인사로 알려진 기업인이나 공직자일수록 청렴결백한 인물로 존경받는다. 기업에서 역능 있는 간부들은 직원들을 다스림에 있어서 공과 사가 정확하며 거래처와 관계유지에도 소득 점을 무지무지 중시한다.
 ​
 시고로 모습을 보여줄 식사 부하직원들로서는 상의물론 그를 따를 수밖에 없다. 공과 사를 철저하게 구분하는 엄격한 관리는 자하 아침에 되지 않는다. 이는 개인의 성격이나 욕심과 깊은 연관을 맺고 있어서 오랜 세상 길거리 길들여지면 직접 고치기가 쉽지 않다.
 잘되는 사람의 성공습관
 요즘에는 아예 사적인 이야기는 교량 않는다.

 슬며시 언제부터인가 말수가 적어졌는데, 머 예전에도 말이 많지는 않았다.

  사적인 이야기를 무진히 해서 물건 좋을것도 없다.

 ​
 굳이 하더라도 그렇게까지 신경쓰는 사람도 참말 벌료 없다.

 그러므로 굳이 말할 필요가 없다. 회사에서는 오로지 일.

 그렇다고 너무너무 일적인 이야기만 하는것도
  딱딱한 사람으로 낙인 찍 힐수 있으니 정도가 중요하다 본다.
 출처:https://pin.it/7KF93a2
 소문에 의존하지 마라
 ​
 마케팅 컨설팅 회사인 '폭스&코'의 간판 제프리 폭스가 쓴 'How to become CEO'에 소개된 75가지 불법 중심 하나로 '권력 다툼에 휘말리지 마라'라는 말이 있다. 그는 소문에 귀머거리가 될 것이며, 유혹에 넘어가지 말고 사람들이 하는 기요 이야기에 무심하라고 말한다. 다시 소문을 퍼뜨리지도 말고 동의하지도 말라고 당부한다.
 ​
 소자본 창업을 하는 사람들 중에는 '어떤 장사가 언제나 된다더라','한달에 얼마를 번다더라'는 '더라', 대뜸 소문만 믿고 뛰어 들었다가 한두 해도 못 버티고 문을 닫는 이들이 적지 않다.
 ​
 기위 금 되는 지역으로 소문이 자자한 지역은 그쯤 함정도 많기 그러니까 뒤늦게 뛰어들었다가는 본전도 못 챙길 확률이 높은 것이다.
 ​
 각별히 사업을 이끌어가는 CEO들은 소문이나 남의 말에 흔들리지 말아야 한다. '귀가 얇은 사람은 사업하기 어렵다'는 말도 그래서 생겨난 말이다.
 ​
 사장이든 연예인이든 스포츠스타든 정치인이든 의지와 추진력이 강하고 항상 당당한 모습으로 비춰지는 사람들에게는 공통점이 있다. 그들은 '남의 눈치도 볼 필수 없다. 내가 증명하지 않은 소문은 귀담아 듣지 않는다'가 곧 그것이다.

 잘되는 사람의 성공습관
 누가 뭐라 하든지 눈치보며 살기에는 시방 하루도 바쁘다. 나이 더더군다나 소문에 의장 아래가지 않는다. 듣고 싶지도 않고 들어봤자 분위기 귀만 아프다. 게다가 그걸 말한 상대방도 찝찝해 한다. 두귀로 사뭇 듣고 한입으로 적게 말하라 대정코 근본이다. 소문은 듣지도 말고 믿지도 말고 성시 묵묵히 낌새 갈 길만 갈뿐이다.
 출처:https://pin.it/6xGsCWc
 쉽게 가볍게 통지 마라
 ​
 우리 국민들은 2002년 월드컵 4강 신화와 같은 새로운 '기적'을 기대했지만 어쩌면 그것은 무리였는지도 모른다. 그럼에도 국민들의 가슴속엔 어쩌면2002년 월드컵에서 맛본 감동가 환희가 아직도 살아 있다는 얘기다.
 ​
 개최국에서 홈그라운드 이점이 있다지만 감히 누가 당시 대한민국 축구가 월드컵4강 신화를 만들 거라고 생각했겠는가?
 ​
 '경적필매'라고 했다. 적을 가볍게 보면 정히 패배할 수밖에 없다는 것이다. 이유는 극한 가지다. 상대를 매우 쉽게 여기고 자만한 것이다. 자만이 지나치다 보면 긴장이 풀리고 의욕이나 열정도 넘쳐나지 않는다. 그렇지만 상대는 다르다. 버거운 적이기 그렇게 어떻게 해서든 최선의 노력을 다해야 한다는 각오뿐이다.
 ​
 이길 고갱이 있다는 자만이나 과욕보다는 할 수있는 데까지 최선을 다한다는 마음가짐으로 경기에 임하다 보니 자신조차 믿을 핵심 없는 좋은 결과를 얻게 되는 것이다.
 ​
 스포츠 경기만이 아니다. 재미있는 얘기로 학창시절을 회상해 보자. 관해 못한다고 무시당했던 친구, 미모가 떨어져서 미팅에만 나가면 으레 친구들에게 멋진 상대를 빼앗겨야 했던 친구가 있었다.
 20년 사후 동창회 모임에서 이들을 잼처 만나면 상황은 다르다.
 ​
 성적이 중간에도 못 들던 친구가 대학교수가 되어 있고, 얼굴이 예쁘지 않았던 친구는 연예인만큼이나 세련되고 우아한여인이 되어 있다.
 ​
 무시하던 사람들은 결미 패자나 다름없는 처지가 된 셈이다.

 세상은 어느 분야든 경쟁의 축으로 흘러간다.
 다만, 선의의 경쟁을 해야 하며, 제각각 최선의 노력을 기울인 내종 승리든 패배든 결과에 승복하는 아름다운 자세를 보여주어야 한다.
 ​
 오만한 자세는 결코 자제해야한다. 자신이 승자라고 할지라도 패자의 능력도 인정해 주는 마인드가 필요하다. 상대를 가볍게 보거나 우습게 여기지 말아야 한다. 영원한 승자란 없는 법이니까.
 잘되는 사람의 성공습관
 예전에 UFC 라이트급 우승자 이었던 맥그리거의 패배가 생각이 난다. 맥그리거는 말빨이 좋고 스타성과 활동력 모두를 겸비한 격투기 선수이다.

 그때 러시아 파이터 하빕과의 경기가 있었는데, 맥그리거가 졌다.
 ​
  물론, 맥그리거는 지금도 최고고 패더급 최강의 실력자인데

 말빨이라는게 사실.. 오만한것도 있었다.

 마땅히 관객들이 볼때는 최고의 환호성이긴 하지만..

 언젠가는 뒷바라지 않을까 싶었는데 조마조마했었다.
  물론, 하빕의 승리예상이 많았었다. 

 기밀은 목숨처럼 지녀야 한다
 ​
 손자병법에서는 적에 대한 물자 활동이 누설될 정세 그것을 들은사람과 말한 사해형제 송두리째 사형에 처해야 한다고 이른다. 말한 자는 죄를 다스리는 것이고 들은 자는 너 말이 새삼스레 전파되는 것을 막기 위한 것이다.
 ​
 치열한 경쟁과 복잡한 구조로 이루어진 사회는 전쟁터와 같아. 해야 될 것과 해도 될것, 더욱이 해서는 가인 될 것들이 있다. 기업의 조직이나 개인의 사생활에서도 꼭 지켜야 하는, 세상에 지켜주어야 하는 비밀이 있다.
 ​
 반대로 불구하고 국가적인 불가사의 기업의 기밀이 외부로 새어나가는 일이 각양각색 발생하고 있다. 남달리 컴퓨터 사용이 일반화되면서 이러한 일은 더 늘어나고 있는 추세다.
 ​
 요새 들어서는 인터넷을 통해 연예인의 사생활에 대한 유포나 소문은 물론이고, 개인의 사생활과 관련된 비밀이나 동영상자료 유포가 사회적인 문제로 대두되고 있다.
 ​
 국가 기밀이든 회사 개인의 비밀이든 관계 당사자들은 딱 지켜주어야 한다. 비단 그행위 자체가 범죄이기 때문만은 아니다. 인간으로서 지켜야 할 도의적인 책임이 포함되어 있기 때문이다.

 ​
 잘되는 사람의 성공습관
 남의 사생활에 대한 이야기를 사적으로 듣다보면 불쾌해지는 경우가 혹 있다. 찝찝한 경우도 있다. 듣고 싶어서 들은건 아니지만, 이따금 듣게 되면 그렇다. 몰래 모든 문제는 모조리 입에서 시작됨을 무지 느낀다.

 ​
 그리고, 공장에서도 누출되어서는 안되는 것들이 있는데, 공부할때 그것을 찍거나 메모해서도 안된다고 한다. 여하간 정형 공장안에서만 공부해야하고 설령, 숙소에서 공부하더라도 머릿속에 기억된 정보로 공부해야한다고 하니 여 만큼 기밀은 중요한거다. 개인의 노총 방식 당연 지켜주어야한다.

 출처:https://pin.it/6WKQoK3
 <작가 소개>
 ​
 지은이 박창수
 ​
 1991년 부터 취재기자를 거쳐, 1997년 밥벌이 프리랜서로 출발하여 지금은 14년 경력의 '기업나라' '마케팅퍼커스' 시사교육매거진 등의 신문, 잡지, 교육 취재기자로 동작 중이며, 실용, 경제도서 작가로서 중국, 일본, 호주 등 현지 취재를 통해 적발 및 단행본 집필활동을 하고 있다.
 ​
 ​
 ​
 <관련뉴스>
 ​
 <들으면 좋은 음악>
 ​
 잘되는 사람의 성공습관
 ​
